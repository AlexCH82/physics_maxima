# Создать список номеров задач
txt = ""
# Открытие файла задач wxm
with open("task.wxm", 'r') as f:
    k1 = "skip"
    for l in f:
        k2 = k1
        # Поиск ключевых слов "comment start"
        if l.find("comment start") != -1:
            k1 = "task"
        else: # Иначе пропустить всё остальное
            k1 = "skip"
        # Добавление строки, следующей за "comment start"
        if k2 == "task":
            txt += l

# Удаление первой строки с комментариями
txt = txt.split("\n")[1:]
txt = "\n".join(txt)

# Запись результата в файл
with open("task.list", 'w') as f:
    f.write(txt)
